## Avant-propos

- Merci à GL de Etalab pour son aide.

- _Pour aller plus en avant dans cette installation avec cette procédure, une pratique de la ligne de commande est indispensable._

- Rappel du lien vers la procédure de l'équipe de PIAF (Etalab) : https://github.com/etalab/piaf

- L'installation en local, idéalement dans une machine virtuelle, n'est pas obligatoire. Pour les personnes qui, comme moi, ne sont pas des experts en administration de système et sur Docker, c'est l'occasion de pratiquer, avant déploiement définitif sur un serveur, la procédure d'installation ainsi que l'administration de la plateforme.

- La procédure propose une installation avec et sans Docker (et Docker-Compose). L'installation avec Docker et Docker-Compose est fortement recommandé et facile. Notamment pour son adaptation à différentes configurations et pour sa mise à jour. L'installation de Docker et de Docker-Compose est proposée ci-dessous. 

### Installation de Docker et Docker-compose

* Installer Docker CE et Docker-Compose : https://doc.ubuntu-fr.org/docker

## Installation de PIAFANNO en local dans VB (avec Docker)

- L'installation a été faite sur Ubuntu Desktop 20.04.2. Cela a parfaitement fonctionné. PC portable i3 et 12 GB de RAM.

- Suivre la procédure ici  : https://github.com/etalab/piaf

- Lancer la commande (à partir de home) `git clone https://github.com/etalab/piaf.git`.

- Un répertoire `piaf` a été créé, aller dedans avec `cd piaf`.

- Pour la commande d'environnement, voir vers la fin de ce document. À ce stade, ce n'est pas utile (d'autant plus qu'il faut lancer cette commande dans le répertoire concerné).

- Lancer l'installation proprement dite avec la commande Docker-Compose : `docker-compose pull`. 
  - Comme indiqué dans la procédure, cela demande un certain temps (~ 3 à 5 mn). 
  - Au début, des lignes de code apparaissent. Attendre la suite de l'installation, ne pas faire de `ctrl c`.
  - L'installation s'arrête sur le message `piaf_webpack_1 exited with code 0` et conserve le curseur. Laisser comme ceci et poursuivre la procédure.
  - Garder votre terminal visible, des messages comme par exemple la création de compte ou la réinitialisation de mot de passe vont apparaître.

  - À ce stade-là, un `Ctrl C` arrêtera l'application et PIAFANNO ne sera plus accessible :). Voir plus loin le chapitre sur la gestion de l'application.

  - L'application s'est installée dans : `~$ cd piaf` (dépend de l'endroit où vous avez lancé le clone)

  - Pour la gestion de l'application et sa désinstallation, voir le chapitre plus loin.

## Accéder à l'interface d'administration de PIAF avec le navigateur

- Entrer dans le navigateur de la même machine l'url suivante : `http://127.0.0.1:8000/login/`.


> Les adresses à retenir (marques-pages) 
> - `http://127.0.0.1:8000/login/` (Si rentré dans un compte directement, faire déconnexion pour revenir à cet écran)
> - `http://127.0.0.1:8000/admin` : administration des comptes (admin et utilisateurs)
> - `http://127.0.0.1:8000/app/admin` Administration des textes (entrer de nouveaux textes ou exporter les textes annotés)
>    - Note : l'interface ne présente pas d'outils pour supprimer un texte. Il faut aller dans l'administration du site.


- La page de connexion apparaît.

- Comme précisé dans la procédure, utiliser les codes suivants : 
  - utilisateur : "admin", 
  - mot de passe : "password".
  - Cliquer sur `Se connecter`

- IMPORTANT. Réinitialisation d'un mot de passe. Dans cette configuration, la connexion au courriel n'a pas été effectuée. Elle est cependant possible par l'envoi de messages dans le terminal.
Pour `réinitialiser` un mot de passe :
  - Cliquer sur `Réinitialiser`, 
  - Entrer votre adresse de courriel lorsque demandé,
  - Cliquer sur le bouton `Réinitialiser`
  - **Consulter le message envoyé dans le terminal**. Sur le lien proposé de la forme `http://127.0.0.1:8000/reset/Mg/5pa-c7...443ce4/`, cliquer dessus avec la touche Ctrl enfoncée (dépend des machines)). Attention, si copie du lien, pas de `Ctrl C` dans le terminal. Mais plutôt : Clic droit sur le lien puis `Copy Link` dans le menu contextuel.

![PIAF Interface de login de l'administration](https://raw.githubusercontent.com/etalab/piaf/master/doc/signup.png)
Document Etalab.

- La page d'accueil apparaît. 
  - Les catégorie ne sont pas encore accessibles car vous n'avez pas encore connecté le texte d'entrée.
  - Le menu du haut regroupe des liens vers le site d'Etalab. 
  - En Haut et à droite apparaît l'icône de login.  

- En cliquant sur l'icône de login, un message de rappel de votre compte admin (temporaire) d'installation apparaît. Il est possible de se déconnecter à l'aide du bouton `déconnecter` dans le bas de ce rappel. 

## Texte (format .json) d'entrée de PIAFANNO (input dataset) 

- Préparer votre fichier texte d'entrée (.json) qui servira de base pour les questions. Un fichier d'exemple de la catégorie "Histoire" (détails ci-dessous) est proposé au téléchargement : [Télécharger un texte .jason d'exemple](https://github.com/etalab/piaf/blob/master/input-dataset-example.json "Télécharger un texte .json d'exemple")

- Télécharger votre texte (format .json) à partir de cette page : `http://127.0.0.1:8000/app/admin`.
  - Ne pas confondre : `/admin`, administration du site, et `/app/admin`, administration de l'application d'annotations.

- C'est ce texte au format .json que les utilisateurs vont annoter.

- Remarques : le "texte", au format .jason, dispose dans son arborescence de "Catégories". Ce sont celles que nous voyons apparaitre sur la page d'accueil de l'utilisateur. Les icônes actives seront celles qui disposent de leur catégorie dans le fichier .json.

- Exemple de "fichier" d'entrée de PIAFANNO
  - A noter, vers la fin du texte, la `"categorie": "Histoire"`
  - Ce texte sera accessible grâce à l'icône `Histoire` qui sera activée.

```
{
  "version": "v2.0",
  "data": [{
    "title": "Sport",
    "paragraphs": [{
      "context": "La Gr\u00e8ce, Rome, Byzance, l'Occident m\u00e9di\u00e9val puis moderne, mais aussi l'Am\u00e9rique pr\u00e9colombienne  ou l'Asie, sont tous marqu\u00e9s par l'importance du sport. Certaines p\u00e9riodes sont surtout marqu\u00e9es par des interdits concernant le sport, comme c'est le cas en Grande-Bretagne du Moyen \u00c2ge \u00e0 l'\u00e9poque Moderne. Interrog\u00e9e sur la question, la Justice anglaise tranche ainsi en 1748 que le cricket n\u2019est pas un jeu ill\u00e9gal. Ce sport, comme tous les autres, figurait en effet sur des \u00e9dits royaux d'interdiction r\u00e9guli\u00e8rement publi\u00e9s par les monarques britanniques du XIe  au  XVe si\u00e8cle. En 1477, la pratique d'un \u00ab jeu interdit \u00bb est ainsi passible de trois ans de prison. Malgr\u00e9 l'interdit, la pratique perdure, n\u00e9cessitant un rappel quasi permanent \u00e0 la r\u00e8gle.",
      "qas": []
    }],
    "categorie": "Histoire",
    "displaytitle": "Premier combat naval de Tripoli",
    "wikipedia_page_id": 7138870
  }]
}
```

- A chaque fois qu'un fichier "texte" est téléchargé, il écrase complètement le fichier précédent.

- Le texte téléchargé peut être disponible pour les utilisateurs "Certifiés" ou "non certifiés". Voir la liste déroulante "Restriction des utilisateurs".

- Cliquer sur le bouton "Ajouter ces articles" pour activer le fichier sélectionné.

## Dataset de sortie (au format SQuAD/FQuAD)

Format "SQuAD du fichier. ??? A confirmer. Le mêême que QuAD-FR ???
```
{
  "data": [
    {
      "title": "Sport",
      "categorie": "Société",
      "wikipedia_page_id": "0",
      "audience": "restricted",
      "paragraphs": [
        {
          "context": "La Grèce, Rome, Byzance, l'Occident médiéval puis moderne, mais aussi l'Amérique précolombienne  ou l'Asie, sont tous marqués par l'importance du sport. Certaines périodes sont surtout marquées par des interdits concernant le sport, comme c'est le cas en Grande-Bretagne du Moyen Âge à l'époque Moderne. Interrogée sur la question, la Justice anglaise tranche ainsi en 1748 que le cricket n’est pas un jeu illégal. Ce sport, comme tous les autres, figurait en effet sur des édits royaux d'interdiction régulièrement publiés par les monarques britanniques du XIe  au  XVe siècle. En 1477, la pratique d'un « jeu interdit » est ainsi passible de trois ans de prison. Malgré l'interdit, la pratique perdure, nécessitant un rappel quasi permanent à la règle.",
          "qas": [
            {
              "question": "Malgré l'interdit qu'est-ce qu'il faut sans cesse rappeler ?",
              "id": 5,
              "answers": [
                {
                  "text": "règle",
                  "answer_start": 748
                },
                {
                  "text": "règle",
                  "answer_start": 748
                }
              ]
            },
            {
              "question": "Qui indique en 1748 que le cricket n'est pas un jeu illégal ?",
              "id": 4,
              "answers": [
                {
                  "text": "Justice anglaise",
                  "answer_start": 335
                },
                {
                  "text": "Justice anglaise",
                  "answer_start": 335
                }
              ]
            },
```


## Gestion de l'application et désinstallation

- L'accès de l'application avec le navigateur se fait uniquement si l'application a été lancée. Si le navigateur ne trouve plus l'application, c'est que celle-ci a été arrêtée.

- Pour arrêter l'application, dans le répertoire `Piaf`, lanceer la commande `make down`. 

- Pour la redémarrer l'application, dans le répertoire `Piaf`, lancer la commande `make up`. 
  - **IMPORTANT**: Attendre le message au terminal : `piaf_webpack_1 exited with code 0`. Sinon des messages d'erreur peuvent apparaître comme : `TemplateDoesNotExist at /app/`

- IMPORTANT : Pas de `Ctrl C` dans le terminal (comme pour une copie :). Un `Ctrl C` arrête l'application.

- Pour la désinstallation :
  - Supprimer le répertoire `Piaf`, pour supprimer l'application.
  - Pour supprimer les éléments liés à Docker, voir avec les commandes Docker selon votre configuration.


## Suite à déconnecter

- Si vous avez maîtrisé l'installation, il ne vous reste plus qu'à installer PIAFANNO sur un serveur pour permettre au plus grand nombre de contribuer.

- N'hésitez à remonter des informations et bugs par des `Issues`.
** 
