> PIAF : Pour une Intelligence Articifielle Francophone. Open Source. Développé par Etalab : https://piaf.etalab.studio/
> PIAFANNO est la plateforme qui permet l'annotations (fine-training) afin de "muscler" le modèle de PIAF pour son application.

# Proposer une instance indépendante (mais à l'image) de PIAFANNO (Etalab).

# Une instance PIAF indépendante ? Pourquoi ?

Objectif : la promotion et la pratique de l'annotation d'IA Open Source Francophone par l'installation et la mise à disposition du grand public d'une instance d'annotations PIAF web, PIAFANNO, open source.
Voir l'intérêt pour toute la francophonie.
L'objectif est de susciter des vocations et des services basés sur des déploiements "opérationnel" de PIAF.

Le but ultime étant de disposer d'outils IA francophones "natifs" comme alternance aux IA des grandes multinationales.

Cette instance indépendante est proposée suite à la fin de la mise à disposition de la plateforme publique d'annotation de PIAF (PIAFANNO). [Clap de fin de la première saison](https://piaf.etalab.studio/fin "Clap de fin de la première saison").

Aucune connaissance en Intelligence Artificielle n'est requise. L'apprche du projet est de fonctionner avec des "boîtes noires" en liaison avec les experts.


# Références

- La plateforme web d'annotations : https://github.com/etalab/piaf

- Le dataset existant (500 contributeurs, 9 225 questions-réponses) : https://www.data.gouv.fr/fr/datasets/piaf-le-dataset-francophone-de-questions-reponses/
Il est bien sûûr possible d'utiliser PIAF avec son propre dataset.

- PIAF déployé ici : https://huggingface.co/etalab-ia/camembert-base-squadFR-fquad-piaf

- Pour poser les questions à PIAF : https://github.com/etalab-ia/piaf_agent

- Mise en oeuvre de chatbots, du cas d'usage à la mise en place de l'interface, L'usine à chatbots : https://github.com/fabnumdef/chatbot-template.


# Installation

### [Installation de PIAFANNO en local](https://gitlab.com/jrd10/PIAFANNO-Instance-Independante/-/blob/master/Installation-Local-VB.md "Installation de PIAFANNO en local") 

- Pour découvrir la plateforme et son administration.

### Installation de PIAFANNO sur un serveur

- Pour une large contribution.

- À venir (mai 2021).

### Créer un chatbot avec PIAF

- À venir (mai 2021)


# Venez participer, contribuer, utiliser

- Toutes personnes intéressées par rejoindre le projet (open source), quelque soit sa motivation, peut me contacter sur GitLab @jrd10.

- N'étant pas administrateur système, j'apprécierai des contributions dans ce domaine :). 


# Le futur (26 mars 2021)

1. Installation sur un PC personnel pour premiers tests

2. Installation sur un serveur avec un nom de domaine

    2.1 Nom "commercial" à trouver

    2.2 Licence à définir

    2.3 Constituer une équipe pour une gestion de qualité de la plateforme

    2.4 Voir pour les budgets :) 

    2.5 Étendre le projet à la francophonie

3. Réaliser un tuto pour faciliter la prise en main + la diffusion

4. Contribuer à des projets de chatbots Open Source Francophone (natif) et autres :).

# N'hésitez pas à partager

- _Merci de votre temps._

- _Merci à GL data scientiste de l'équipe PIAF_ 
 
